﻿using System;
using System.Windows;
using System.Windows.Controls;
using Entities;
using Logic;

namespace iPaperExercise1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly ICalculationRepo _calculationRepo = CalculationRepo.Instance;
        private uint _slBrochuresValue;
        private uint _slVisitorsValue;
        private Tuple<uint, uint> _totalCostAndVisitors;
        private const string Prefix = "€";
        private const string Affix = "/month";

        public MainWindow()
        {
            InitializeComponent();
            
            _slBrochuresValue = (uint)SlBrochures.Minimum;
            _slVisitorsValue = (uint)SlVisitors.Minimum;

            _totalCostAndVisitors = _calculationRepo.CalculateTotalCost((uint) SlBrochures.Minimum, (uint) SlVisitors.Minimum);

            LblTotalCost.Content = $"{Prefix}{_totalCostAndVisitors.Item1}{Affix}";
        }

        private void SlVisitors_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var visitorsCount = (uint) ((Slider) sender).Value;
            LblVisitors.Content = visitorsCount;

            _slVisitorsValue = visitorsCount;

            UpdateTotalCost();
        }

        private void SlBrochures_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var brochureCount = (uint) ((Slider) sender).Value;
            LblBrochures.Content = brochureCount;

            _slBrochuresValue = brochureCount;

            UpdateTotalCost();
        }

        private void UpdateTotalCost()
        {
            if (SlBrochures != null && SlVisitors != null)
            {
                var brochureCount = (uint)SlBrochures.Value;
                var visitorsCount = (uint)SlVisitors.Value;

                if (LblTotalCost != null)
                {
                    _totalCostAndVisitors = _calculationRepo.CalculateTotalCost(brochureCount, visitorsCount);
                    LblTotalCost.Content = $"{Prefix}{_totalCostAndVisitors.Item1}{Affix}";
                }
            }
        }

        // Using a standard click event. If there were conditions for when the button 
        // could be clicked, I would have used a RelayCommand.
        private void BtnPlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            var order = new Order
            {
                Brochures = _slBrochuresValue,
                Visitors = _totalCostAndVisitors.Item2,
                TotalCost = _totalCostAndVisitors.Item1
            };

            // Imaginary saving of order to persistent storage.
            MessageBox.Show(order.ToString());
        }
    }
}
