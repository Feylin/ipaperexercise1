﻿using System;

namespace Logic
{
    public interface ICalculationRepo
    {
        /// <summary>
        /// Method used to calculate the total cost of selected number of brochures and selected number of visitors.
        /// </summary>
        /// <param name="brochures">Number of brochures.</param>
        /// <param name="visitors">Number of visitors.</param>
        /// <returns>A tuple containing two items, where item1 is the total cost of brochures and visitors, and item2 is the total number of visitors.</returns>
        Tuple<uint, uint> CalculateTotalCost(uint brochures = 1, uint visitors = 1000);
    }
}