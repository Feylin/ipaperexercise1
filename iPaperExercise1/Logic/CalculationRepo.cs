﻿using System;
using System.Collections.Concurrent;
using Entities;

namespace Logic
{
    public sealed class CalculationRepo : ICalculationRepo
    {
        private static readonly Lazy<CalculationRepo> Lazy = new Lazy<CalculationRepo>(() => new CalculationRepo());

        public static CalculationRepo Instance => Lazy.Value;

        private CalculationRepo()
        {
        }

        private static readonly ConcurrentDictionary<uint, Tuple<uint, uint>> BrochureCost = new ConcurrentDictionary<uint, Tuple<uint, uint>>();
        private static readonly ConcurrentDictionary<uint, uint> VisitorsCost = new ConcurrentDictionary<uint, uint>();
        private static readonly ConcurrentDictionary<ulong, Tuple<uint, uint>> TotalCost = new ConcurrentDictionary<ulong, Tuple<uint, uint>>();

        // Tuple might be suitable enough as a dictionary key.
        // Use pairing funciton as key?
        // The two methods besides this one are only public for testing purposes.
        // The calls should go through the interface, to only expose this method to the callers.
        public Tuple<uint, uint> CalculateTotalCost(uint brochures, uint visitors)
        {
            var calculationKey = UniqueKey(brochures, visitors);
            Tuple<uint, uint> totalValueTuple;

            if (TotalCost.TryGetValue(calculationKey, out totalValueTuple))
                return totalValueTuple;

            var intermediateCostAndVisitors = CalculateBrochureCost(brochures);

            var totalCost = intermediateCostAndVisitors.Item1 + CalculateVisitorsCost(visitors);
            var totalVisitors = intermediateCostAndVisitors.Item2 + visitors;

            totalValueTuple = Tuple.Create(totalCost, totalVisitors);

            TotalCost[calculationKey] = totalValueTuple;

            // item1 = totalCost, item2 = totalVisitors
            return totalValueTuple;
        }

        private static ulong UniqueKey(uint x, uint y)
        {
            ulong id = x > y ? y | ((ulong)x << 32) :
                               x | ((ulong)y << 32);
            return id;
        }

        private static long PairingFunction(int a, int b)
        {
            var A = (ulong)(a >= 0 ? 2 * (long)a : -2 * (long)a - 1);
            var B = (ulong)(b >= 0 ? 2 * (long)b : -2 * (long)b - 1);
            var C = (long)((A >= B ? A * A + A + B : A + B * B) / 2);

            return a < 0 && b < 0 || a >= 0 && b >= 0 ? C : -C - 1;
        }

        /// <summary>
        /// Method used to calculate the cost of the selected number of brochures.
        /// </summary>
        /// <param name="brochures">Number of brochures.</param>
        /// <returns>A tuple containing the total cost of the selected number of brochures as item1, and the included number of visitors as item2.</returns>
        public Tuple<uint, uint> CalculateBrochureCost(uint brochures)
        {
            Tuple<uint, uint> tuple;

            uint totalBrochureCost = 0;
            uint totalBrochureVisitors = 0;

            if (BrochureCost.TryGetValue(brochures, out tuple))
                return tuple;

            const int lowerLimit = 5;
            const int midLimit = 10;
            const int upperLimit = 25;

            // Not happy about this solution but it works until I figure out something better.
            for (int i = 1; i <= brochures; i++)
            {
                if (i <= lowerLimit)
                {
                    totalBrochureCost += 39;
                    totalBrochureVisitors += 2000;
                }
                if (i > lowerLimit && i <= midLimit)
                {
                    totalBrochureCost += 19;
                    totalBrochureVisitors += 1000;
                }
                if (i > midLimit && i <= upperLimit)
                {
                    totalBrochureCost += 9;
                    totalBrochureVisitors += 250;
                }
                if (i > upperLimit)
                {
                    totalBrochureCost += 3;
                    totalBrochureVisitors += 250;
                }
            }

            tuple = Tuple.Create(totalBrochureCost, totalBrochureVisitors);

            BrochureCost[brochures] = tuple;

            return tuple;
        }

        /// <summary>
        /// Calculate the cost of selected number of visitors.
        /// </summary>
        /// <param name="visitors">Number of visitors.</param>
        /// <returns>The total cost of the selected number of visitors in the form of an unsigned integer.</returns>
        public uint CalculateVisitorsCost(uint visitors)
        {
            uint totalVisitorsCost;

            if (VisitorsCost.TryGetValue(visitors, out totalVisitorsCost))
                return totalVisitorsCost;

            // There is no particular reason for the use of Math.Power, besides the fact that I think it looks better than the number equivalents.
            // There is obviously a small performance hit by doing it this way, instead of writing
            // 1.000, 10.000 and 50.000.
            const double startValue = 10e2;
            const double lowerLimit = 10e3;
            const double midLimit = 50e3;

            for (double i = startValue; i <= visitors; i += startValue)
            {
                if (i <= lowerLimit)
                    totalVisitorsCost += 10;
                if (i > lowerLimit && i <= midLimit)
                    totalVisitorsCost += 7;
                if (i > midLimit)
                    totalVisitorsCost += 6;
            }

            VisitorsCost[visitors] = totalVisitorsCost;

            return totalVisitorsCost;
        }
    }
}
