﻿namespace Entities
{
    public struct CalculationKey
    {
        public uint Brochures { get; }
        public uint Visitors { get; }

        public CalculationKey(uint brochures, uint visitors)
        {
            Brochures = brochures;
            Visitors = visitors;
        }
    }
}
