﻿using System.Threading;

namespace Entities
{
    public class Order
    {
        private static int _orderCount;
        public int OrderCount { get; }
        public uint Brochures { get; set; }
        public uint Visitors { get; set; }
        public uint TotalCost { get; set; }

        public Order()
        {
            OrderCount = Interlocked.Increment(ref _orderCount);
        }

        protected bool Equals(Order other)
        {
            return Brochures == other.Brochures && Visitors == other.Visitors && TotalCost == other.TotalCost;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Order) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) Brochures;
                hashCode = (hashCode * 397) ^ (int) Visitors;
                hashCode = (hashCode * 397) ^ (int) TotalCost;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"Order #: {OrderCount}, {nameof(Brochures)}: {Brochures}, Expected visitors: {Visitors}, Total cost: £{TotalCost}/month";
        }
    }
}
