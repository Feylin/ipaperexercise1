﻿using System;
using Logic;
using NUnit.Framework;

namespace UnitTests
{
    [TestFixture]
    public class CalculationRepoTests
    {
        private CalculationRepo _calculationRepo;

        [TestFixtureSetUp]
        public void SetUp()
        {
            _calculationRepo = CalculationRepo.Instance;
        }

        // The parameterized test cases are written as an equation because it makes it easier to compare with the assigment.
        // Brochure edge cases.
        [TestCase(1, 39, 2000)]
        [TestCase(5, 39 * 5, 2000 * 5)]
        [TestCase(6, 39 * 5 + 19, 2000 * 5 + 1000)]
        [TestCase(10, 39 * 5 + 19 * 5, 2000 * 5 + 1000 * 5)]
        [TestCase(11, 39 * 5 + 19 * 5 + 9, 2000 * 5 + 1000 * 5 + 250)]
        [TestCase(25, 39 * 5 + 19 * 5 + 9 * 15, 2000 * 5 + 1000 * 5 + 250 * 15)]
        [TestCase(26, 39 * 5 + 19 * 5 + 9 * 15 + 3, 2000 * 5 + 1000 * 5 + 250 * 16)]
        [TestCase(100, 39 * 5 + 19 * 5 + 9 * 15 + 3 * 75, 2000 * 5 + 1000 * 5 + 250 * 90)]
        public void CalculateBrochureCostTests(int brochureCount, int expectedBrochureCost, int expectedVisitors)
        {
            // item1 is brochure cost, item2 is the visitors included with the number of brochures.
            var actualResultTuple = _calculationRepo.CalculateBrochureCost((uint) brochureCount);
            var expectedResultTuple = Tuple.Create((uint) expectedBrochureCost, (uint) expectedVisitors);

            Assert.AreEqual(actualResultTuple, expectedResultTuple);
        }

        // Visitors edge cases.
        [TestCase(1000, 10)]
        [TestCase(10000, 10 * 10)]
        [TestCase(11000, 10 * 10 + 7)]
        [TestCase(50000, 10 * 10 + 7 * 40)]
        [TestCase(51000, 10 * 10 + 7 * 40 + 6)]
        [TestCase(100000, 10 * 10 + 7 * 40 + 6 * 50)]
        public void CalculateVisitorsCostTests(int visitorsCount, int expectedVisitorsCost)
        {
            var actualVisitorsCost = _calculationRepo.CalculateVisitorsCost((uint) visitorsCount);

            Assert.AreEqual(actualVisitorsCost, expectedVisitorsCost);
        }
    }
}
